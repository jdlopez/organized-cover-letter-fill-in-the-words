/* Author: Jose D. Lopez */

So you want to make applying for jobs a little bit easier...

Well I felt the same at one point so I whipped up this script that you can use.

I've only was able to parse Latex files, so if you are using word, then this wont work. Feel free to send me a msg to u/Josegrowl on reddit if you have any questions or suggestions for improvement.
You need to install MacTeX from here to automatically convert tex to pdf, if you dont want to then remove the second to last line
	pdflatex "$FILE"

This is a ridged script so you will need to edit it to fit your needs. I had no intention of sharing it, but many people have asked for a copy, so here it is

The way this script works is by parsing the COVER_LETTER.tex file, where COVER_LETTER is whatever you named it, and looking for lines that contain "\textbf{somewords}"
ex:

\textbf{RECRUITER}\newline

\textbf{RECRUITERTITLE}\newline

\textbf{COMPANY}\newline

\textbf{ADDRESS}

the words in all caps such as RECRUITER will be replaced with what ever you entered when prompted.

Here are the list of all the keywords:
DATE

RECRUITER

RECRUITERTITLE

COMPANY

ADDRESS

WEBSITE - where I found the job applicaion

POSITION

You need to change the path where the .tex file is located for the SRC variable and change the path for where you want the copy cover letter and subfolders created for the DEST variable but LEAVE "/$CN" AT THE END. Also put the cover letter file name for the BASEFILE variable

Now just run jobApp in the terminal and follow the prompts
